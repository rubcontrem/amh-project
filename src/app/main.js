import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

import App from './App.vue';
import Index from './views/Index.vue';
import Home from './views/Home.vue';
import Login from './components/auth/Login.vue';
import Signup from './components/auth/Signup.vue';

const routes = [
    {
        name: 'Index',
        path: '/',
        component: Index,
        meta: {
            title: 'Template'
        }
    },
    {
        name: 'Login',
        path: '/login',
        component: Login,
        meta: {
            title: 'Login || Template'
        }
    },
    {
        name: 'Signup',
        path: '/signup',
        component: Signup,
        meta: {
            title: 'Signup || Template'
        }
    },
    {
        name: 'Home',
        path: '/home',
        component: Home,
        meta: {
            requiresAuth: true,
            title: 'Home || Template'
        }
    }

];

const router = new VueRouter({ mode: 'history', routes: routes });

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (localStorage.getItem("jwt") == null) {
            next({
            path: "/"
            });
        } else {
            next();
        }
    } else {
        next();
    }
});

new Vue(Vue.util.extend({ router }, App)).$mount('#app');

