'use strict'

const express = require('express');
const path = require('path');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
const morgan = require('morgan');
const cors = require('cors');
const config = require("./config/db");
const app = express();

// *** SETTINGS ***
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
// Middlewares
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(cookieParser());
// Static files
app.use(express.static(__dirname + '/public'));

// *** MONGODB ***
// The mongoose module is loaded to connect to MongoDB
var mongoose = require('mongoose');
// Connection to mongoose is made with promises
mongoose.Promise = global.Promise;
// Connect our database
mongoose.connect(config.database, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true })
  .then(() => {
    console.log('The database connection was successful.');
    console.log(config.database);
  })
  .catch(err => console.log(err));

// *** ROUTER ***
var indexRouter = require('./routes/index');
//Index
app.use('/', indexRouter);
//app.use('/users', indexRouter);
//app.use('/login', indexRouter);
//app.use('/signup', indexRouter);



// Server is listening
app.listen(app.get('port'), () => {
  console.log('Server port:', app.get('port'));
});