const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const ROLES = {
    ADMIN: 'ADMIN',
    USER: 'USER'
};

const userSchema = mongoose.Schema({
	username: {
		type: String,
		unique: true,
		required: [true, "Please Include your name"]
	},
	email: {
		type: String,
		unique: true,
		required: [true, "Please Include your email"]
	},
	password: {
		type: String,
		required: [true, "Please Include your password"]
	},
	role: { 
		type: String,
		default: 'USER',
		required:true, 
		enum: Object.values(ROLES) 
	},
	// permissions
	/*
    users_rights: {
        read: {type: Boolean, required: true, default: false},
        write: {type: Boolean, required: true, default: false},
        delete: {type: Boolean, required: true, default: false}
    },*/
	tokens: [
		{
			token: {
				type: String,
				required: true
			}
		}
	]
},
{
    timestamps: true,
});

//this method will hash the password before saving the user model
userSchema.pre("save", async function(next) {
	const user = this;
	if (user.isModified("password")) {
		user.password = await bcrypt.hash(user.password, 8);
	}
	next();
});

//this method generates an auth token for the user
userSchema.methods.generateAuthToken = async function() {
	const user = this;
	const token = jwt.sign({ _id: user._id, username: user.username, email: user.email },
	"secret", {expiresIn: "24h"});
	user.tokens = user.tokens.concat({ token });
	await user.save();
	return token;
};

//this method search for a user by email and password
userSchema.statics.findByCredentials = async (email, password) => {
	//generate a error message if credentials are wrong
	function myError(message) {
		this.message = message;
	}
	myError.prototype = new Error();

	const user = await User.findOne({ email });
	if (!user) {
		throw new myError('Unable to log in. Provide a user. Please check credentials and try again.')
	}

	const isPasswordMatch = await bcrypt.compare(password, user.password)
	if (!isPasswordMatch) {
		throw new myError('Unable to log in. Password does not match. Please check credentials and try again.')
	}
	
	return user;
};

//these methods to know user role
userSchema.methods.isAdmin = function() {
    return this.role==ROLES.ADMIN;
};

userSchema.methods.isUser = function() {
    return this.role==ROLES.USER;
};


const User = mongoose.model('User', userSchema);
module.exports = User;